/**
 * Created by Dmitriy on 23.03.14.
 */

var requestController = (function(){
    var sendRequest = function(type, url, data, callback)
    {
        var xhr = new XMLHttpRequest();
        xhr.open(type, url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onreadystatechange = callback;
        xhr.send(data);
    }

    return {
        sendRequest: sendRequest
    }

})();