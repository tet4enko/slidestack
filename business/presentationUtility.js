/**
 * Created by Dmitriy on 18.03.14.
 */

var S4 = function() {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
};

exports.update = function(from , to)
{
    var parentSlides = from.body ? JSON.parse(from.body)[1] : from.body,
        childSlides = to.body ? JSON.parse(to.body)[1] : to.body,
        i= 1, index ,compareFlag = 0;

    if (parentSlides && childSlides)
    {
        for (var j=0; j < childSlides.length; j++)
        {
            if (!parentSlides[j] || (parentSlides[j].id !== childSlides[j].id) || (parentSlides[j].paperObject !== childSlides[j].paperObject))
            {
                compareFlag = 1;
            }
        }
    }

    if (!compareFlag)
    {
        return 0;
    }

    parentSlides.forEach(function(parentSlide){

        var containsThisSlide = 0, ind;
        childSlides.forEach(function(childSlide, index){
            if (childSlide.id === parentSlide.id)
            {
                containsThisSlide = 1;
                ind = index;
            }
        });

        if (containsThisSlide && (parentSlide.paperObject !== childSlides[ind].paperObject))
        {
            childSlides.splice(ind+1, 0, {
                id: (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase(),
                paperObject: parentSlide.paperObject
            })
        }

        if (!containsThisSlide)
        {
            childSlides.splice(childSlides.length, 0, parentSlide);
        }

    });

    to.body = JSON.stringify([JSON.parse(from.body)[0], childSlides]);
    return 1;
}


exports.getUpdatedBody = function(bodyFrom, bodyTo)
{

    var parentSlides = bodyFrom ? JSON.parse(bodyFrom)[1] : bodyFrom,
        childSlides = bodyTo ? JSON.parse(bodyTo)[1] : bodyTo;



    parentSlides.forEach(function(parentSlide){

        var containsThisSlide = 0, ind;
        childSlides.forEach(function(childSlide, index){
            if (childSlide.id === parentSlide.id)
            {
                containsThisSlide = 1;
                ind = index;
            }
        });

        if (containsThisSlide && (parentSlide.paperObject !== childSlides[ind].paperObject))
        {
            childSlides.splice(ind+1, 0, {
                id: (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase(),
                paperObject: parentSlide.paperObject
            })
        }

        if (!containsThisSlide)
        {
            childSlides.splice(childSlides.length, 0, parentSlide);
        }

    });

    return JSON.stringify([JSON.parse(bodyFrom)[0], childSlides]);
}