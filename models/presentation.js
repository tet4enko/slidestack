
var crypto = require('crypto');

var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    name: {
        type: String,
        required: true
    },
    private: {
        type: Boolean,
        required: true
    },
    body: {
        type: String
    },
    userId: {
        type: String,
        required: true
    },
    preview: {
        type: String
    },
    updated: {
        type: Date,
        default: Date.now
    },
    forked: {
        type : {
            userId : String,
            userName : String,
            presId : String,
            presName : String
        },
        default: {
            userId : '',
            presId : '',
            userName : '',
            presName : ''
        }
    },
    forks: {
        type : [String],
        default: []
    }

});

schema.methods.toSimpleObject = function() {
    return {
        id :  this.get('id'),
        name : this.get('name'),
        private : this.get('private'),
        body : this.get('body'),
        preview : this.get('preview'),
        userId : this.get('userId'),
        updated : this.get('updated'),
        forked : this.get('forked'),
        forks : this.get('forks')
    }
};



exports.Presentation = mongoose.model('Presentation', schema);