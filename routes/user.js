
var async = require('async');
var User = require('../models/user').User;
var Presentation = require('../models/presentation').Presentation;
var PullRequest = require('../models/pullrequest').PullRequest;

exports.get = function(req, res)
{


    async.waterfall([
        function(callback)
        {
            User.findOne({ _id : req.params.userId}, function(err, pageUser){
                callback(err, pageUser ? pageUser.toSimpleObject() : null);
            });
        },
        function(pageUser, callback)
        {
            if (!pageUser)
            {
                callback('error');;
            }
            else
            Presentation.find({ userId : pageUser.id }, function(err, pres)
            {
                callback(err, pres , pageUser)
            });
        },

        function(pageUserPresentations, pageUser, callback)
        {
            PullRequest.find({userToId : pageUser.id}, function(err, pullRequests){
                callback(err, pageUserPresentations, pageUser, pullRequests);
            })
        },

        function(pageUserPresentations, pageUser, pullRequests,  callback)
        {

            pageUserPresentations.forEach(function(item){
               item = item.toSimpleObject();
            });

            pullRequests.forEach(function(item){
                item = item.toSimpleObject();
            });

            if (!req.session.userId)
            {
                res.render('user.ejs', { pageUser : pageUser, pageUserPresentations : pageUserPresentations, pullRequests : pullRequests, currentUser : null });
            }
            else
            {
                User.findOne({ _id : req.session.userId}, function(err, currentUser){
                    callback(err, pageUserPresentations, pageUser, currentUser ? currentUser.toSimpleObject() : null, pullRequests);
                });
            }
        }

    ], function(err, pageUserPresentations, pageUser, currentUser, pullRequests)
    {
        if (err)
        {
            return res.render('error.ejs', {message: 'User not Found'});;
        }
        else
        {
            res.render('user.ejs', { pageUser : pageUser, pageUserPresentations : pageUserPresentations, currentUser : currentUser, pullRequests: pullRequests });
        }
    });

};


exports.createUserPresentation = function(req, res)
{
    if (req.body.data === 'createUserPres')
    {
        var presentation = new Presentation(
            {
                name: 'New Presentation',
                private: false,
                body: '',
                userId: req.params.userId
            }
        );
        presentation.save(function(err, presentation)
        {
            if (err) {
                res.send('error');
            }
            else
            {
                res.send(presentation.get('id'));
            }

        });
    }

}

exports.searchUser = function(req,res)
{
    if (req.body.searchUser)
    {
        async.waterfall([
            function(callback){
                User.find({},function(err, allUsers){
                    callback(err, allUsers);
                });
            }

        ], function(err, allUsers) {
            if (err) {
                res.send('error');
            }
            else
            {
                var searchValue = req.body.searchUser.toLowerCase().replace(/[ ]+/ig,' ').replace(/^\s*/,'').replace(/\s*$/,'');
                resultUsers = [];
                allUsers.forEach(function(item){
                    if ((item.get('mail').toLocaleLowerCase().indexOf(searchValue) >= 0) ||
                        (item.get('firstName').toLocaleLowerCase().indexOf(searchValue) >= 0) ||
                        (item.get('lastName').toLocaleLowerCase().indexOf(searchValue) >= 0) ||
                        ((item.get('firstName') + " " + item.get('lastName')).toLocaleLowerCase().indexOf(searchValue) >= 0))
                    {
                        resultUsers.push(item.toSimpleObject());
                    }
                });
                if (!resultUsers.length)
                {
                    res.send('none');
                }
                else
                {
                    res.send(JSON.stringify(resultUsers));
                }
            }
        })
    }
}